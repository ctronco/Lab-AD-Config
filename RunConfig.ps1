#
# Pick a default password for things and put it here if not already set....
if ($DefaultPassword -eq $null) {
    $DefaultPassword = "VMW@re123!"
}
if ($DAPassword -eq $null){
    $DAPassword = "vagrant"
}
#Specify Domain Name  - If using boxstarter may want to tie to the promo process value
if ($ENV:UserDNSDomain -ne $null) {
    $DomainName = $ENV:UserDNSDomain
} else {
    $DomainName = "example.com"
}


$NetBiosName = $DomainName.Split(".")[0]
$BaseDomainDN = $DomainName.Replace(".",",DC=").Insert(0,"DC=")
$VMwareBaseDN = "OU=VMWare," + $BaseDomainDN
$VMwareGroupsDN = "OU=Groups,"+$VMwareBaseDN
$VMwareUsersDN = "OU=Users,"+ $VMWareBaseDN
$VMwareServersDN = "OU=Servers,"+ $VMWareBaseDN

$DefaultUsersDN = "CN=Users,"+$BaseDomainDN
$DefaultGroupsDN = "CN=Users,"+$BaseDomainDN


$ComputerName = $ENV:ComputerName

. .\Lab_DC_Build.ps1
Lab_DC_Build -ConfigurationData $ConfigurationData -Password

Start-DscConfiguration -Path .\Lab_DC_Build -Wait -Verbose -Force

   Install-WindowsFeature -Name 'RSAT-AD-PowerShell'